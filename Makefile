PREFIX ?= /usr
BIN ?= $(PREFIX)/bin
MAN ?= $(PREFIX)/share/man

.PHONY: install uninstall

all:
	@echo "Nothing to do. Try running \"make install\" to install or \"make uninstall\" to uninstall"

install:
	@install -v -d "$(BIN)"
	@install -v -d "$(MAN)/man1"
	@install -m 0755 -v -D work "$(BIN)"
	@install -v -D work.1 "$(MAN)/man1"

uninstall:
	@rm -vrf "$(BIN)/work"
	@rm -vrf "$(MAN)/man1/work.1"
