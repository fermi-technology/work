# work
A simple Unix project manager that can create, edit, manage, delete, and 
package projects.

![](work.png)

For more information please visit [https://fermitechnology.com/software/work/](https://fermitechnology.com/software/work/).

## Dependencies:
* bash
  [https://www.gnu.org/software/bash/](https://www.gnu.org/software/bash/)
* tree
  [http://mama.indstate.edu/users/ice/tree/](http://mama.indstate.edu/users/ice/tree/)
* awk/gawk
  [https://www.gnu.org/software/gawk/](https://www.gnu.org/software/gawk/)
* tar
  [https://www.gnu.org/software/tar/](https://www.gnu.org/software/tar/)
* zip
  [http://infozip.sourceforge.net/Zip.html](http://infozip.sourceforge.net/Zip.html)

## Install\Uninstall:

To install:
```
sudo make install
```

To uninstall:
```
sudo make uninstall
```

## Contact:
To report any bugs, typos, or anything else, please email:
[bpv@disroot.org](mailto://bpv@disroot.org)
